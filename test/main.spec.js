/*
import { describe, it } from "mocha";
import { expect } from "chai";
import main from "../src/main.js";
*/
const { describe, it } = require('mocha');
const { expect } = require('chai');
const main = require('../src/main.js')
let output = "";
let log = console.log;


describe("Test suite", () => {
    before(() => {
        console.log = (content) => {
            output += content + '\n';
        };
    });
    beforeEach(() => {
        output = "";
    });
    it('No identifier', () =>{
        main(["node", "bouncer.js"]);
        //log(output);
        expect(output).to.equal("No identifier, insert age.\nYou are not allowed to enter!\n");
    });
    it('Faulty identifier', () => {
        // ##input##
        // "9jOxRwG9"
        main(["node", "bouncer.js", "hello"]);
        // ##output##
        // "Invalid identifier."
        // "You are not allowed to enter!"
        expect(output).to.eq("Invalid identifier.\nYou are not allowed to enter!\n");
    });

    it('Underage', () => {
        // ##input##
        // "17"
        main(["node", "bouncer.js", "17"]);
        // ##output##
        // "Under age."
        // "You are not allowed to enter!"
        expect(output).to.eq("Under age.\nYou are not allowed to enter!\n");
    });
    
    it('Legal age', () => {
        // ##input##
        // "18"
        main(["node", "bouncer.js", "18"]);
        // ##output##
        // "Welcome!"
        expect(output).to.eq("Welcome!\n");
    });
});
